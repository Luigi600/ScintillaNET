﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace ScintillaNET
{
    public partial class Scintilla
    {
        // Events
        private static readonly object scNotificationEventKey = new object();
        private static readonly object insertCheckEventKey = new object();
        private static readonly object beforeInsertEventKey = new object();
        private static readonly object beforeDeleteEventKey = new object();
        private static readonly object insertEventKey = new object();
        private static readonly object deleteEventKey = new object();
        private static readonly object updateUIEventKey = new object();
        private static readonly object modifyAttemptEventKey = new object();
        private static readonly object styleNeededEventKey = new object();
        private static readonly object savePointReachedEventKey = new object();
        private static readonly object savePointLeftEventKey = new object();
        private static readonly object changeAnnotationEventKey = new object();
        private static readonly object marginClickEventKey = new object();
        private static readonly object marginRightClickEventKey = new object();
        private static readonly object charAddedEventKey = new object();
        private static readonly object autoCSelectionEventKey = new object();
        private static readonly object autoCCompletedEventKey = new object();
        private static readonly object autoCCancelledEventKey = new object();
        private static readonly object autoCCharDeletedEventKey = new object();
        private static readonly object dwellStartEventKey = new object();
        private static readonly object callTipClickEventKey = new object();
        private static readonly object dwellEndEventKey = new object();
        private static readonly object borderStyleChangedEventKey = new object();
        private static readonly object doubleClickEventKey = new object();
        private static readonly object paintedEventKey = new object();
        private static readonly object needShownEventKey = new object();
        private static readonly object hotspotClickEventKey = new object();
        private static readonly object hotspotDoubleClickEventKey = new object();
        private static readonly object hotspotReleaseClickEventKey = new object();
        private static readonly object indicatorClickEventKey = new object();
        private static readonly object indicatorReleaseEventKey = new object();
        private static readonly object zoomChangedEventKey = new object();

        #region Events

        /// <summary>
        /// Occurs when an autocompletion list is cancelled.
        /// </summary>
        [Category("Notifications")]
        [Description("Occurs when an autocompletion list is cancelled.")]
        public event EventHandler<EventArgs> AutoCCancelled
        {
            add
            {
                Events.AddHandler(autoCCancelledEventKey, value);
            }
            remove
            {
                Events.RemoveHandler(autoCCancelledEventKey, value);
            }
        }

        /// <summary>
        /// Occurs when the user deletes a character while an autocompletion list is active.
        /// </summary>
        [Category("Notifications")]
        [Description("Occurs when the user deletes a character while an autocompletion list is active.")]
        public event EventHandler<EventArgs> AutoCCharDeleted
        {
            add
            {
                Events.AddHandler(autoCCharDeletedEventKey, value);
            }
            remove
            {
                Events.RemoveHandler(autoCCharDeletedEventKey, value);
            }
        }

        /// <summary>
        /// Occurs after autocompleted text is inserted.
        /// </summary>
        [Category("Notifications")]
        [Description("Occurs after autocompleted text has been inserted.")]
        public event EventHandler<AutoCSelectionEventArgs> AutoCCompleted
        {
            add
            {
                Events.AddHandler(autoCCompletedEventKey, value);
            }
            remove
            {
                Events.RemoveHandler(autoCCompletedEventKey, value);
            }
        }

        /// <summary>
        /// Occurs when a user has selected an item in an autocompletion list.
        /// </summary>
        /// <remarks>Automatic insertion can be cancelled by calling <see cref="AutoCCancel" /> from the event handler.</remarks>
        [Category("Notifications")]
        [Description("Occurs when a user has selected an item in an autocompletion list.")]
        public event EventHandler<AutoCSelectionEventArgs> AutoCSelection
        {
            add
            {
                Events.AddHandler(autoCSelectionEventKey, value);
            }
            remove
            {
                Events.RemoveHandler(autoCSelectionEventKey, value);
            }
        }

        /// <summary>
        /// Not supported.
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public new event EventHandler BackColorChanged
        {
            add
            {
                base.BackColorChanged += value;
            }
            remove
            {
                base.BackColorChanged -= value;
            }
        }

        /// <summary>
        /// Not supported.
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public new event EventHandler BackgroundImageChanged
        {
            add
            {
                base.BackgroundImageChanged += value;
            }
            remove
            {
                base.BackgroundImageChanged -= value;
            }
        }

        /// <summary>
        /// Not supported.
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public new event EventHandler BackgroundImageLayoutChanged
        {
            add
            {
                base.BackgroundImageLayoutChanged += value;
            }
            remove
            {
                base.BackgroundImageLayoutChanged -= value;
            }
        }

        /// <summary>
        /// Occurs when text is about to be deleted.
        /// </summary>
        [Category("Notifications")]
        [Description("Occurs before text is deleted.")]
        public event EventHandler<BeforeModificationEventArgs> BeforeDelete
        {
            add
            {
                Events.AddHandler(beforeDeleteEventKey, value);
            }
            remove
            {
                Events.RemoveHandler(beforeDeleteEventKey, value);
            }
        }

        /// <summary>
        /// Occurs when text is about to be inserted.
        /// </summary>
        [Category("Notifications")]
        [Description("Occurs before text is inserted.")]
        public event EventHandler<BeforeModificationEventArgs> BeforeInsert
        {
            add
            {
                Events.AddHandler(beforeInsertEventKey, value);
            }
            remove
            {
                Events.RemoveHandler(beforeInsertEventKey, value);
            }
        }

        /// <summary>
        /// Occurs when the value of the <see cref="Scintilla.BorderStyle" /> property has changed.
        /// </summary>
        [Category("Property Changed")]
        [Description("Occurs when the value of the BorderStyle property changes.")]
        public event EventHandler BorderStyleChanged
        {
            add
            {
                Events.AddHandler(borderStyleChangedEventKey, value);
            }
            remove
            {
                Events.RemoveHandler(borderStyleChangedEventKey, value);
            }
        }

        /// <summary>
        /// Occurs when an annotation has changed.
        /// </summary>
        [Category("Notifications")]
        [Description("Occurs when an annotation has changed.")]
        public event EventHandler<ChangeAnnotationEventArgs> ChangeAnnotation
        {
            add
            {
                Events.AddHandler(changeAnnotationEventKey, value);
            }
            remove
            {
                Events.RemoveHandler(changeAnnotationEventKey, value);
            }
        }

        /// <summary>
        /// Occurs when the user enters a text character.
        /// </summary>
        [Category("Notifications")]
        [Description("Occurs when the user types a character.")]
        public event EventHandler<CharAddedEventArgs> CharAdded
        {
            add
            {
                Events.AddHandler(charAddedEventKey, value);
            }
            remove
            {
                Events.RemoveHandler(charAddedEventKey, value);
            }
        }

        /// <summary>
        /// Not supported.
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public new event EventHandler CursorChanged
        {
            add
            {
                base.CursorChanged += value;
            }
            remove
            {
                base.CursorChanged -= value;
            }
        }

        /// <summary>
        /// Occurs when text has been deleted from the document.
        /// </summary>
        [Category("Notifications")]
        [Description("Occurs when text is deleted.")]
        public event EventHandler<ModificationEventArgs> Delete
        {
            add
            {
                Events.AddHandler(deleteEventKey, value);
            }
            remove
            {
                Events.RemoveHandler(deleteEventKey, value);
            }
        }

        /// <summary>
        /// Occurs when the <see cref="Scintilla" /> control is double-clicked.
        /// </summary>
        [Category("Notifications")]
        [Description("Occurs when the editor is double clicked.")]
        public new event EventHandler<DoubleClickEventArgs> DoubleClick
        {
            add
            {
                Events.AddHandler(doubleClickEventKey, value);
            }
            remove
            {
                Events.RemoveHandler(doubleClickEventKey, value);
            }
        }

        /// <summary>
        /// Occurs when the mouse moves or another activity such as a key press ends a <see cref="DwellStart" /> event.
        /// </summary>
        [Category("Notifications")]
        [Description("Occurs when the mouse moves from its dwell start position.")]
        public event EventHandler<DwellEventArgs> DwellEnd
        {
            add
            {
                Events.AddHandler(dwellEndEventKey, value);
            }
            remove
            {
                Events.RemoveHandler(dwellEndEventKey, value);
            }
        }

        /// <summary>
        /// Occurs when the mouse clicked over a call tip displayed by the <see cref="CallTipShow" /> method.
        /// </summary>
        [Category("Notifications")]
        [Description("Occurs when the mouse is clicked over a calltip.")]
        public event EventHandler<CallTipClickEventArgs> CallTipClick
        {
            add
            {
                Events.AddHandler(callTipClickEventKey, value);
            }
            remove
            {
                Events.RemoveHandler(callTipClickEventKey, value);
            }
        }


        /// <summary>
        /// Occurs when the mouse is kept in one position (hovers) for the <see cref="MouseDwellTime" />.
        /// </summary>
        [Category("Notifications")]
        [Description("Occurs when the mouse is kept in one position (hovers) for a period of time.")]
        public event EventHandler<DwellEventArgs> DwellStart
        {
            add
            {
                Events.AddHandler(dwellStartEventKey, value);
            }
            remove
            {
                Events.RemoveHandler(dwellStartEventKey, value);
            }
        }

        /// <summary>
        /// Not supported.
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public new event EventHandler FontChanged
        {
            add
            {
                base.FontChanged += value;
            }
            remove
            {
                base.FontChanged -= value;
            }
        }

        /// <summary>
        /// Not supported.
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public new event EventHandler ForeColorChanged
        {
            add
            {
                base.ForeColorChanged += value;
            }
            remove
            {
                base.ForeColorChanged -= value;
            }
        }

        /// <summary>
        /// Occurs when the user clicks on text that is in a style with the <see cref="Style.Hotspot" /> property set.
        /// </summary>
        [Category("Notifications")]
        [Description("Occurs when the user clicks text styled with the hotspot flag.")]
        public event EventHandler<HotspotClickEventArgs> HotspotClick
        {
            add
            {
                Events.AddHandler(hotspotClickEventKey, value);
            }
            remove
            {
                Events.RemoveHandler(hotspotClickEventKey, value);
            }
        }

        /// <summary>
        /// Occurs when the user double clicks on text that is in a style with the <see cref="Style.Hotspot" /> property set.
        /// </summary>
        [Category("Notifications")]
        [Description("Occurs when the user double clicks text styled with the hotspot flag.")]
        public event EventHandler<HotspotClickEventArgs> HotspotDoubleClick
        {
            add
            {
                Events.AddHandler(hotspotDoubleClickEventKey, value);
            }
            remove
            {
                Events.RemoveHandler(hotspotDoubleClickEventKey, value);
            }
        }

        /// <summary>
        /// Occurs when the user releases a click on text that is in a style with the <see cref="Style.Hotspot" /> property set.
        /// </summary>
        [Category("Notifications")]
        [Description("Occurs when the user releases a click on text styled with the hotspot flag.")]
        public event EventHandler<HotspotClickEventArgs> HotspotReleaseClick
        {
            add
            {
                Events.AddHandler(hotspotReleaseClickEventKey, value);
            }
            remove
            {
                Events.RemoveHandler(hotspotReleaseClickEventKey, value);
            }
        }

        /// <summary>
        /// Occurs when the user clicks on text that has an indicator.
        /// </summary>
        [Category("Notifications")]
        [Description("Occurs when the user clicks text with an indicator.")]
        public event EventHandler<IndicatorClickEventArgs> IndicatorClick
        {
            add
            {
                Events.AddHandler(indicatorClickEventKey, value);
            }
            remove
            {
                Events.RemoveHandler(indicatorClickEventKey, value);
            }
        }

        /// <summary>
        /// Occurs when the user releases a click on text that has an indicator.
        /// </summary>
        [Category("Notifications")]
        [Description("Occurs when the user releases a click on text with an indicator.")]
        public event EventHandler<IndicatorReleaseEventArgs> IndicatorRelease
        {
            add
            {
                Events.AddHandler(indicatorReleaseEventKey, value);
            }
            remove
            {
                Events.RemoveHandler(indicatorReleaseEventKey, value);
            }
        }

        /// <summary>
        /// Occurs when text has been inserted into the document.
        /// </summary>
        [Category("Notifications")]
        [Description("Occurs when text is inserted.")]
        public event EventHandler<ModificationEventArgs> Insert
        {
            add
            {
                Events.AddHandler(insertEventKey, value);
            }
            remove
            {
                Events.RemoveHandler(insertEventKey, value);
            }
        }

        /// <summary>
        /// Occurs when text is about to be inserted. The inserted text can be changed.
        /// </summary>
        [Category("Notifications")]
        [Description("Occurs before text is inserted. Permits changing the inserted text.")]
        public event EventHandler<InsertCheckEventArgs> InsertCheck
        {
            add
            {
                Events.AddHandler(insertCheckEventKey, value);
            }
            remove
            {
                Events.RemoveHandler(insertCheckEventKey, value);
            }
        }

        /// <summary>
        /// Occurs when the mouse was clicked inside a margin that was marked as sensitive.
        /// </summary>
        /// <remarks>The <see cref="Margin.Sensitive" /> property must be set for a margin to raise this event.</remarks>
        [Category("Notifications")]
        [Description("Occurs when the mouse is clicked in a sensitive margin.")]
        public event EventHandler<MarginClickEventArgs> MarginClick
        {
            add
            {
                Events.AddHandler(marginClickEventKey, value);
            }
            remove
            {
                Events.RemoveHandler(marginClickEventKey, value);
            }
        }


        // TODO This isn't working in my tests. Could be Windows Forms interfering.
        /// <summary>
        /// Occurs when the mouse was right-clicked inside a margin that was marked as sensitive.
        /// </summary>
        /// <remarks>The <see cref="Margin.Sensitive" /> property and <see cref="PopupMode.Text" /> must be set for a margin to raise this event.</remarks>
        /// <seealso cref="UsePopup(PopupMode)" />
        [Category("Notifications")]
        [Description("Occurs when the mouse is right-clicked in a sensitive margin.")]
        public event EventHandler<MarginClickEventArgs> MarginRightClick
        {
            add
            {
                Events.AddHandler(marginRightClickEventKey, value);
            }
            remove
            {
                Events.RemoveHandler(marginRightClickEventKey, value);
            }
        }

        /// <summary>
        /// Occurs when a user attempts to change text while the document is in read-only mode.
        /// </summary>
        /// <seealso cref="ReadOnly" />
        [Category("Notifications")]
        [Description("Occurs when an attempt is made to change text in read-only mode.")]
        public event EventHandler<EventArgs> ModifyAttempt
        {
            add
            {
                Events.AddHandler(modifyAttemptEventKey, value);
            }
            remove
            {
                Events.RemoveHandler(modifyAttemptEventKey, value);
            }
        }

        /// <summary>
        /// Occurs when the control determines hidden text needs to be shown.
        /// </summary>
        /// <remarks>An example of when this event might be raised is if the end of line of a contracted fold point is deleted.</remarks>
        [Category("Notifications")]
        [Description("Occurs when hidden (folded) text should be shown.")]
        public event EventHandler<NeedShownEventArgs> NeedShown
        {
            add
            {
                Events.AddHandler(needShownEventKey, value);
            }
            remove
            {
                Events.RemoveHandler(needShownEventKey, value);
            }
        }

        internal event EventHandler<SCNotificationEventArgs> SCNotification
        {
            add
            {
                Events.AddHandler(scNotificationEventKey, value);
            }
            remove
            {
                Events.RemoveHandler(scNotificationEventKey, value);
            }
        }

        /// <summary>
        /// Not supported.
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public new event PaintEventHandler Paint
        {
            add
            {
                base.Paint += value;
            }
            remove
            {
                base.Paint -= value;
            }
        }

        /// <summary>
        /// Occurs when painting has just been done.
        /// </summary>
        [Category("Notifications")]
        [Description("Occurs when the control is painted.")]
        public event EventHandler<EventArgs> Painted
        {
            add
            {
                Events.AddHandler(paintedEventKey, value);
            }
            remove
            {
                Events.RemoveHandler(paintedEventKey, value);
            }
        }

        /// <summary>
        /// Occurs when the document becomes 'dirty'.
        /// </summary>
        /// <remarks>The document 'dirty' state can be checked with the <see cref="Modified" /> property and reset by calling <see cref="SetSavePoint" />.</remarks>
        /// <seealso cref="SetSavePoint" />
        /// <seealso cref="SavePointReached" />
        [Category("Notifications")]
        [Description("Occurs when a save point is left and the document becomes dirty.")]
        public event EventHandler<EventArgs> SavePointLeft
        {
            add
            {
                Events.AddHandler(savePointLeftEventKey, value);
            }
            remove
            {
                Events.RemoveHandler(savePointLeftEventKey, value);
            }
        }

        /// <summary>
        /// Occurs when the document 'dirty' flag is reset.
        /// </summary>
        /// <remarks>The document 'dirty' state can be reset by calling <see cref="SetSavePoint" /> or undoing an action that modified the document.</remarks>
        /// <seealso cref="SetSavePoint" />
        /// <seealso cref="SavePointLeft" />
        [Category("Notifications")]
        [Description("Occurs when a save point is reached and the document is no longer dirty.")]
        public event EventHandler<EventArgs> SavePointReached
        {
            add
            {
                Events.AddHandler(savePointReachedEventKey, value);
            }
            remove
            {
                Events.RemoveHandler(savePointReachedEventKey, value);
            }
        }

        /// <summary>
        /// Occurs when the control is about to display or print text and requires styling.
        /// </summary>
        /// <remarks>
        /// This event is only raised when <see cref="Lexer" /> is set to <see cref="ScintillaNET.Lexer.Container" />.
        /// The last position styled correctly can be determined by calling <see cref="GetEndStyled" />.
        /// </remarks>
        /// <seealso cref="GetEndStyled" />
        [Category("Notifications")]
        [Description("Occurs when the text needs styling.")]
        public event EventHandler<StyleNeededEventArgs> StyleNeeded
        {
            add
            {
                Events.AddHandler(styleNeededEventKey, value);
            }
            remove
            {
                Events.RemoveHandler(styleNeededEventKey, value);
            }
        }

        /// <summary>
        /// Occurs when the control UI is updated as a result of changes to text (including styling),
        /// selection, and/or scroll positions.
        /// </summary>
        [Category("Notifications")]
        [Description("Occurs when the control UI is updated.")]
        public event EventHandler<UpdateUIEventArgs> UpdateUI
        {
            add
            {
                Events.AddHandler(updateUIEventKey, value);
            }
            remove
            {
                Events.RemoveHandler(updateUIEventKey, value);
            }
        }

        /// <summary>
        /// Occurs when the user zooms the display using the keyboard or the <see cref="Zoom" /> property is changed.
        /// </summary>
        [Category("Notifications")]
        [Description("Occurs when the control is zoomed.")]
        public event EventHandler<EventArgs> ZoomChanged
        {
            add
            {
                Events.AddHandler(zoomChangedEventKey, value);
            }
            remove
            {
                Events.RemoveHandler(zoomChangedEventKey, value);
            }
        }

        #endregion Events

        /// <summary>
        /// Raises the <see cref="AutoCCancelled" /> event.
        /// </summary>
        /// <param name="e">An EventArgs that contains the event data.</param>
        protected virtual void OnAutoCCancelled(EventArgs e) =>
            (Events[autoCCancelledEventKey] as EventHandler<EventArgs>)?.Invoke(this, e);


        /// <summary>
        /// Raises the <see cref="AutoCCharDeleted" /> event.
        /// </summary>
        /// <param name="e">An EventArgs that contains the event data.</param>
        protected virtual void OnAutoCCharDeleted(EventArgs e) =>
            (Events[autoCCharDeletedEventKey] as EventHandler<EventArgs>)?.Invoke(this, e);


        /// <summary>
        /// Raises the <see cref="AutoCCompleted" /> event.
        /// </summary>
        /// <param name="e">An <see cref="AutoCSelectionEventArgs" /> that contains the event data.</param>
        protected virtual void OnAutoCCompleted(AutoCSelectionEventArgs e) =>
            (Events[autoCCompletedEventKey] as EventHandler<AutoCSelectionEventArgs>)?.Invoke(this, e);

        /// <summary>
        /// Raises the <see cref="AutoCSelection" /> event.
        /// </summary>
        /// <param name="e">An <see cref="AutoCSelectionEventArgs" /> that contains the event data.</param>
        protected virtual void OnAutoCSelection(AutoCSelectionEventArgs e) =>
            (Events[autoCSelectionEventKey] as EventHandler<AutoCSelectionEventArgs>)?.Invoke(this, e);


        /// <summary>
        /// Raises the <see cref="BeforeDelete" /> event.
        /// </summary>
        /// <param name="e">A <see cref="BeforeModificationEventArgs" /> that contains the event data.</param>
        protected virtual void OnBeforeDelete(BeforeModificationEventArgs e) =>
            (Events[beforeDeleteEventKey] as EventHandler<BeforeModificationEventArgs>)?.Invoke(this, e);


        /// <summary>
        /// Raises the <see cref="BeforeInsert" /> event.
        /// </summary>
        /// <param name="e">A <see cref="BeforeModificationEventArgs" /> that contains the event data.</param>
        protected virtual void OnBeforeInsert(BeforeModificationEventArgs e) =>
            (Events[beforeInsertEventKey] as EventHandler<BeforeModificationEventArgs>)?.Invoke(this, e);

        /// <summary>
        /// Raises the <see cref="BorderStyleChanged" /> event.
        /// </summary>
        /// <param name="e">An EventArgs that contains the event data.</param>
        protected virtual void OnBorderStyleChanged(EventArgs e) =>
            (Events[borderStyleChangedEventKey] as EventHandler)?.Invoke(this, e);


        /// <summary>
        /// Raises the <see cref="ChangeAnnotation" /> event.
        /// </summary>
        /// <param name="e">A <see cref="ChangeAnnotationEventArgs" /> that contains the event data.</param>
        protected virtual void OnChangeAnnotation(ChangeAnnotationEventArgs e) =>
            (Events[changeAnnotationEventKey] as EventHandler<ChangeAnnotationEventArgs>)?.Invoke(this, e);


        /// <summary>
        /// Raises the <see cref="CharAdded" /> event.
        /// </summary>
        /// <param name="e">A <see cref="CharAddedEventArgs" /> that contains the event data.</param>
        protected virtual void OnCharAdded(CharAddedEventArgs e) =>
            (Events[charAddedEventKey] as EventHandler<CharAddedEventArgs>)?.Invoke(this, e);


        /// <summary>
        /// Raises the <see cref="Delete" /> event.
        /// </summary>
        /// <param name="e">A <see cref="ModificationEventArgs" /> that contains the event data.</param>
        protected virtual void OnDelete(ModificationEventArgs e) =>
            (Events[deleteEventKey] as EventHandler<ModificationEventArgs>)?.Invoke(this, e);


        /// <summary>
        /// Raises the <see cref="DoubleClick" /> event.
        /// </summary>
        /// <param name="e">A <see cref="DoubleClickEventArgs" /> that contains the event data.</param>
        protected virtual void OnDoubleClick(DoubleClickEventArgs e) =>
            (Events[doubleClickEventKey] as EventHandler<DoubleClickEventArgs>)?.Invoke(this, e);


        /// <summary>
        /// Raises the <see cref="DwellEnd" /> event.
        /// </summary>
        /// <param name="e">A <see cref="DwellEventArgs" /> that contains the event data.</param>
        protected virtual void OnDwellEnd(DwellEventArgs e) =>
            (Events[dwellEndEventKey] as EventHandler<DwellEventArgs>)?.Invoke(this, e);


        /// <summary>
        /// Raises the <see cref="DwellStart" /> event.
        /// </summary>
        /// <param name="e">A <see cref="DwellEventArgs" /> that contains the event data.</param>
        protected virtual void OnDwellStart(DwellEventArgs e) =>
            (Events[dwellStartEventKey] as EventHandler<DwellEventArgs>)?.Invoke(this, e);


        /// <summary>
        /// Raises the <see cref="CallTipClick" /> event.
        /// </summary>
        /// <param name="e">A <see cref="CallTipClickEventArgs" /> that contains the event data.</param>
        protected virtual void OnCallTipClick(CallTipClickEventArgs e) =>
            (Events[callTipClickEventKey] as EventHandler<CallTipClickEventArgs>)?.Invoke(this, e);


        /// <summary>
        /// Raises the <see cref="HotspotClick" /> event.
        /// </summary>
        /// <param name="e">A <see cref="HotspotClickEventArgs" /> that contains the event data.</param>
        protected virtual void OnHotspotClick(HotspotClickEventArgs e) =>
            (Events[hotspotClickEventKey] as EventHandler<HotspotClickEventArgs>)?.Invoke(this, e);


        /// <summary>
        /// Raises the <see cref="HotspotDoubleClick" /> event.
        /// </summary>
        /// <param name="e">A <see cref="HotspotClickEventArgs" /> that contains the event data.</param>
        protected virtual void OnHotspotDoubleClick(HotspotClickEventArgs e) =>
            (Events[hotspotDoubleClickEventKey] as EventHandler<HotspotClickEventArgs>)?.Invoke(this, e);


        /// <summary>
        /// Raises the <see cref="HotspotReleaseClick" /> event.
        /// </summary>
        /// <param name="e">A <see cref="HotspotClickEventArgs" /> that contains the event data.</param>
        protected virtual void OnHotspotReleaseClick(HotspotClickEventArgs e) =>
            (Events[hotspotReleaseClickEventKey] as EventHandler<HotspotClickEventArgs>)?.Invoke(this, e);


        /// <summary>
        /// Raises the <see cref="IndicatorClick" /> event.
        /// </summary>
        /// <param name="e">An <see cref="IndicatorClickEventArgs" /> that contains the event data.</param>
        protected virtual void OnIndicatorClick(IndicatorClickEventArgs e) =>
            (Events[indicatorClickEventKey] as EventHandler<IndicatorClickEventArgs>)?.Invoke(this, e);


        /// <summary>
        /// Raises the <see cref="IndicatorRelease" /> event.
        /// </summary>
        /// <param name="e">An <see cref="IndicatorReleaseEventArgs" /> that contains the event data.</param>
        protected virtual void OnIndicatorRelease(IndicatorReleaseEventArgs e) =>
            (Events[indicatorReleaseEventKey] as EventHandler<IndicatorReleaseEventArgs>)?.Invoke(this, e);


        /// <summary>
        /// Raises the <see cref="Insert" /> event.
        /// </summary>
        /// <param name="e">A <see cref="ModificationEventArgs" /> that contains the event data.</param>
        protected virtual void OnInsert(ModificationEventArgs e) =>
            (Events[insertEventKey] as EventHandler<ModificationEventArgs>)?.Invoke(this, e);


        /// <summary>
        /// Raises the <see cref="InsertCheck" /> event.
        /// </summary>
        /// <param name="e">An <see cref="InsertCheckEventArgs" /> that contains the event data.</param>
        protected virtual void OnInsertCheck(InsertCheckEventArgs e) =>
            (Events[insertCheckEventKey] as EventHandler<InsertCheckEventArgs>)?.Invoke(this, e);


        /// <summary>
        /// Raises the <see cref="MarginClick" /> event.
        /// </summary>
        /// <param name="e">A <see cref="MarginClickEventArgs" /> that contains the event data.</param>
        protected virtual void OnMarginClick(MarginClickEventArgs e) =>
            (Events[marginClickEventKey] as EventHandler<MarginClickEventArgs>)?.Invoke(this, e);


        /// <summary>
        /// Raises the <see cref="MarginRightClick" /> event.
        /// </summary>
        /// <param name="e">A <see cref="MarginClickEventArgs" /> that contains the event data.</param>
        protected virtual void OnMarginRightClick(MarginClickEventArgs e) =>
            (Events[marginRightClickEventKey] as EventHandler<MarginClickEventArgs>)?.Invoke(this, e);


        /// <summary>
        /// Raises the <see cref="ModifyAttempt" /> event.
        /// </summary>
        /// <param name="e">An EventArgs that contains the event data.</param>
        protected virtual void OnModifyAttempt(EventArgs e) =>
            (Events[modifyAttemptEventKey] as EventHandler<EventArgs>)?.Invoke(this, e);


        /// <summary>
        /// Raises the <see cref="NeedShown" /> event.
        /// </summary>
        /// <param name="e">A <see cref="NeedShownEventArgs" /> that contains the event data.</param>
        protected virtual void OnNeedShown(NeedShownEventArgs e) =>
            (Events[needShownEventKey] as EventHandler<NeedShownEventArgs>)?.Invoke(this, e);


        /// <summary>
        /// Raises the <see cref="Painted" /> event.
        /// </summary>
        /// <param name="e">An EventArgs that contains the event data.</param>
        protected virtual void OnPainted(EventArgs e) =>
            (Events[paintedEventKey] as EventHandler<EventArgs>)?.Invoke(this, e);


        /// <summary>
        /// Raises the <see cref="SavePointLeft" /> event.
        /// </summary>
        /// <param name="e">An EventArgs that contains the event data.</param>
        protected virtual void OnSavePointLeft(EventArgs e) =>
            (Events[savePointLeftEventKey] as EventHandler<EventArgs>)?.Invoke(this, e);


        /// <summary>
        /// Raises the <see cref="SavePointReached" /> event.
        /// </summary>
        /// <param name="e">An EventArgs that contains the event data.</param>
        protected virtual void OnSavePointReached(EventArgs e) =>
            (Events[savePointReachedEventKey] as EventHandler<EventArgs>)?.Invoke(this, e);


        /// <summary>
        /// Raises the <see cref="StyleNeeded" /> event.
        /// </summary>
        /// <param name="e">A <see cref="StyleNeededEventArgs" /> that contains the event data.</param>
        protected virtual void OnStyleNeeded(StyleNeededEventArgs e) =>
            (Events[styleNeededEventKey] as EventHandler<StyleNeededEventArgs>)?.Invoke(this, e);


        /// <summary>
        /// Raises the <see cref="UpdateUI" /> event.
        /// </summary>
        /// <param name="e">An <see cref="UpdateUIEventArgs" /> that contains the event data.</param>
        protected virtual void OnUpdateUI(UpdateUIEventArgs e) =>
            (Events[updateUIEventKey] as EventHandler<UpdateUIEventArgs>)?.Invoke(this, e);


        /// <summary>
        /// Raises the <see cref="ZoomChanged" /> event.
        /// </summary>
        /// <param name="e">An EventArgs that contains the event data.</param>
        protected virtual void OnZoomChanged(EventArgs e) =>
            (Events[zoomChangedEventKey] as EventHandler<EventArgs>)?.Invoke(this, e);

    }
}